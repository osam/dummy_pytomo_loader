import os
from shared_modules import read_config
from prepare_data import *
from main import tomography
from make_graphs import * # for plotting

### Configure the paths and load the inputs dictionary 
cfg_file = "tomography"
program_path = os.path.abspath(__file__)
program_path = program_path[:program_path.rfind('/')]+'/'        
local_path = os.path.expanduser('~/tomography/')
inputs = read_config(local_path+cfg_file+".cfg")
 
inputs['program_path']= program_path
inputs['local_path']  = local_path
inputs['output_path'] = os.path.expanduser(os.path.expandvars(inputs['output_path']))+'/'
inputs['tmp_folder']  = os.path.expanduser(os.path.expandvars(inputs['tmp_folder']))+'/'

config.wrong_dets_pref = inputs['wrong_dets']
if 'usecache' in inputs:
    config.useCache = inputs['usecache']


### Manual data entry
inputs['shot'] = int(30579)
inputs['tmin'] = 3.2008
inputs['tmax'] = 3.2015  
inputs['tok_index'] = 10 # 9: ASDEX SXR, 10: ASDEX SXRfast
inputs['plot_all'] = False  # separated plot for each time step
inputs['gnuplot'] = True # plot with gnuplot (faster than in matplotlib)



### Load and prepare the tokamak data (geometry, SXR configs)
tok = loaddata(inputs)
tok.prepare_tokamak()

### Initiate tomography calculations
inputs, tokamak, progress,output = tomography(inputs,tok)

### Plotting 
#make_graphs((inputs, tokamak, progress, output), plot_svd=False)
# plots are saved in ~/tomography/tmp/
