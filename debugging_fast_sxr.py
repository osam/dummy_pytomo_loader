#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import numpy as np
import aug_sfutils as sf
from collections import OrderedDict


shot = int(30579)
tmin = 3.2008
tmax = 3.2015 

# signal names
csx = sf.SFREAD( 'CSX', shot)
names = csx.objects + csx.parsets
signals = [b[1:] for b in names if b[0]== 'C']


# dictionary of DAS and signals
shotfiles = []
for s in signals:
    parset = csx('C' + s)
    shotfiles.append(sf.str_byt.to_str(  b''.join(parset['SX_DIAG']) ) )


SXR_diods = OrderedDict()
for sfile in np.unique(shotfiles):
    SXR_diods[sfile] = []

for sfile, sig in zip(shotfiles, signals):
    #remove T camera and null signals
    if (sig[0] != 'T') and (sfile != 'OOO'):
        sfo = sf.SFREAD(sfile, shot)
        test_fast_sxr_data = sfo.getobject(sig,cal=False, nbeg=0, nend=1)
        #remove T camera and NoneType signals
        if (test_fast_sxr_data != None):
            SXR_diods[sfile].append(sig)
                
SXR_diods.pop('OOO')  #null detector 

# time
sxa = sf.SFREAD('SXA', shot)
if not sxa.status:
    raise Exception('SXA shotfile is not avalible')
tvec = sxa('Time')

#sfo = sf.SFREAD("SXA", shot)
#test = sfo.getobject('I_043', cal=False, nbeg=6401600, nend=6403000+1)
#test = sfo.getobject('J_015', cal=False, nbeg=6401600, nend=6403000+1)
# test2 = sfo.getareabase('I_043', tbeg=None, tend=None) # nothing
#test2 = sfo.getlist('I_043')
# test == None
# test = sfo.getobject("I_043",cal=False, nbeg=0, nend=1)
# del SXR_diods['SXN'][0] # delete an element


for DAS, signals in SXR_diods.items():

    if len(signals) == 0:
        continue
        
    sfo = sf.SFREAD(DAS, shot)
    tb = sfo('Time')
    tbeg = tb[0]
    tend = tb[-1]
    tlen = len(tb)
    
    
    imin,imax = (np.r_[tmin, tmax] - tbeg)/(tend - tbeg)*(tlen - 1)
    imin,imax = int(np.ceil(imin)), int(np.ceil(min(imax, tlen)))

    data_tvec = tb[imin: imax+1] # [osam] original code
    
    
    nt = len(data_tvec)
    
    #load data
    print("DAS:%s"%DAS)
    data = np.ma.zeros((nt, len(signals)), dtype=np.int16)
    for i,sig in enumerate(signals):
        try: # [osam] testing
            data[:, i] = sfo.getobject(sig, cal=False, nbeg=imin, nend=imax+1)
        except Exception as e: # [osam] testing
            print("i, sig = %g, %s" %(i,sig))
            print("imin, imax = %g, %g" %(imin,imax))
            print(e)
            print("!!!Exception!!!")
            print(data[:, i])
